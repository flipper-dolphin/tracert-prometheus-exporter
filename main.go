package main

import (
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var hosts = []string{
	"google.com",
	"cloudflare.com",
}

type TraceStats struct {
	Host string
}

type TraceStatsCollector struct {
	TraceStats *TraceStats
}

func (tt TraceStatsCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(tt, ch)
}
func (tt TraceStatsCollector) Collect(ch chan<- prometheus.Metric) {

	hops := dnsLookupSingle(tt.TraceStats.Host)

	log.Printf("Collecting %s", tt.TraceStats.Host)

	var asn []int64
	var ips []net.Addr

	for _, hop := range hops {
		asn = append(asn, hop.AS)
		ips = append(ips, hop.Addr)
	}

	ch <- prometheus.MustNewConstMetric(
		asn_path,
		prometheus.GaugeValue,
		1,
		fmt.Sprintf("%v", asn),
		fmt.Sprintf("%v", ips),
	)

	ch <- prometheus.MustNewConstMetric(
		traceHops,
		prometheus.GaugeValue,
		float64(len(hops)),
		tt.TraceStats.Host,
	)

}

var (
	traceHops = prometheus.NewDesc(
		"trace_hops",
		"hops of each trace",
		[]string{"trace_host"}, nil,
	)
	asn_path = prometheus.NewDesc(
		"trace",
		"ASN list of each trace",
		[]string{"asn_path", "host_path"},
		nil,
	)
)

func NewTraceStats(host string, reg prometheus.Registerer) *TraceStats {
	t := &TraceStats{
		Host: host,
	}
	tt := TraceStatsCollector{TraceStats: t}
	prometheus.WrapRegistererWith(prometheus.Labels{"host": host}, reg).MustRegister(tt)
	return t
}

func main() {
	reg := prometheus.NewRegistry()

	for _, host := range hosts {
		NewTraceStats(host, reg)
	}

	// Add the standard process and Go metrics to the custom registry.
	reg.MustRegister(
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
		collectors.NewGoCollector(),
	)

	http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	log.Fatal(http.ListenAndServe(":9101", nil))

}
