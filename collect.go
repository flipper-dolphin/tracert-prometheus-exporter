package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/kanocz/tracelib"
)

func dnsLookup(host string) [][]tracelib.MHop {
	dnscache := tracelib.NewLookupCache()
	rawHops, err := tracelib.RunMultiTrace(host, "0.0.0.0", "", time.Second, 64, dnscache, 5, nil)
	if err != nil {
		log.Fatal(err)
	}
	hops := tracelib.AggregateMulti(rawHops)

	for i, hop := range hops {
		isd := fmt.Sprintf("%d. ", i+1)
		isp := strings.Repeat(" ", len(isd))

		for j, h := range hop {
			prefix := isd
			if j > 0 {
				prefix = isp
			}

			fmt.Printf("%s%v(%s)/AS%d %v/%v/%v (final:%v lost %d of %d)\n",
				prefix, h.Host, h.Addr, h.AS, h.MinRTT, h.AvgRTT, h.MaxRTT, h.Final, h.Lost, h.Total)
		}
	}
	return hops
}

func dnsLookupSingle(host string) []tracelib.Hop {
	log.Printf("dnsLookupSingle")
	dnscache := tracelib.NewLookupCache()
	hops, err := tracelib.RunTrace(host, "0.0.0.0", "", time.Second, 64, dnscache, nil)
	if err != nil {
		log.Fatal(err)
	}

	// for i, hop := range hops {
	// 	fmt.Printf("%d. %v(%s)/AS%d %v (final:%v timeout:%v error:%v)\n",
	// 		i+1, hop.Host, hop.Addr, hop.AS, hop.RTT, hop.Final, hop.Timeout, hop.Error)
	// }
	return hops
}
