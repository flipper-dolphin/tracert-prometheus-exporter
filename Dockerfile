FROM golang:1.20

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /docker-tracert-exporter

CMD ["/docker-tracert-exporter"]

EXPOSE 9101

# Run
CMD ["/docker-tracert-exporter"]