# tracert-prometheus-exporter
I had some weird behaviour around latency where it would occasionally drop by a decent amount and go back up. Easiest way to overlap this data in Grafana was to make an exporter. This isn't the right type of data that should really be stored in Prometheus but here we are.

oh ya, currently hardcoded to do google and cloudflare... oops


run the exporter with NET_ADMIN so it can run the traceroute
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: tracert-exporter
  namespace: monitoring
  labels:
    app: tracert-exporter
spec:
  containers:
    - name: tracert-exporter
      image: registry.gitlab.com/flipper-dolphin/tracert-prometheus-exporter:latest
      securityContext:
        capabilities:
          add: ["NET_ADMIN"]
      resources:
        limits:
          cpu: "0.5"
          memory: "128Mi"
        requests:
          cpu: "0.1"
          memory: "64Mi"
      ports:
        - containerPort: 9101
          name: http
          protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  name: tracert-exporter
  namespace: monitoring
spec:
  ports:
    - name: http
      port: 9101
      protocol: TCP
      targetPort: 9101
  selector:
    app: tracert-exporter
  type: ClusterIP

```


scrape the target

```yaml
- job_name: "tracert_exporter"
  scrape_interval: 10m
  static_configs:
    - targets: [
        "tracert-exporter.monitoring.svc.cluster.local:9101"
      ]
```


contributions are welcome if for some reason you want to use this